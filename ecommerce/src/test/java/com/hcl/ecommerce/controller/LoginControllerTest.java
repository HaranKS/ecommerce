package com.hcl.ecommerce.controller;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.hcl.ecommerce.dto.LoginRequestDto;
import com.hcl.ecommerce.dto.LoginResponseDto;
import com.hcl.ecommerce.dto.UserDto;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.service.LoginService;

@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {

	@InjectMocks
	LoginController loginController;

	@Mock
	LoginService loginService;

	@Test
	public void loginTest() throws UserRegistrationException {
		
		LoginRequestDto loginRequest = new LoginRequestDto();
		loginRequest.setEmailId("anu@gmail.com");
		loginRequest.setPassword("123");
		
		UserDto userDto = new UserDto();
		userDto.setName("Anuradha");
		userDto.setTypeId(1);
		
		LoginResponseDto loginResponseDto = new LoginResponseDto();
		loginResponseDto.setMessage("success");
		loginResponseDto.setStatusCode(607);
		loginResponseDto.setUserDto(userDto);
		
		Mockito.when(loginService.userLogin(Mockito.any())).thenReturn(loginResponseDto);
		ResponseEntity<LoginResponseDto> actual = loginController.userLogin(loginRequest);
		assertNotNull(actual);
	}

}
