package com.hcl.ecommerce.controller;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.ecommerce.dto.RegisterRequestDto;
import com.hcl.ecommerce.dto.ResponseDto;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.service.RegisterService;

@RunWith(MockitoJUnitRunner.class)
public class RegisterControllerTest {

	@InjectMocks
	RegisterController registerController;

	@Mock
	RegisterService userService;

	@Test
	public void registerTest() throws UserRegistrationException {
		RegisterRequestDto registerRequestDto = new RegisterRequestDto();
		registerRequestDto.setName("Haran");
		registerRequestDto.setEmailId("str@gmail.com");
		registerRequestDto.setPassword("haran");
		registerRequestDto.setTypeId(0);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		Mockito.when(userService.register(registerRequestDto)).thenReturn(responseDto);
		ResponseEntity<ResponseDto> result = registerController.register(registerRequestDto);
		assertNotNull(result);
		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

}