package com.hcl.ecommerce.controller;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.hcl.ecommerce.dto.ProductDto;
import com.hcl.ecommerce.entity.Product;
import com.hcl.ecommerce.exception.ProductException;
import com.hcl.ecommerce.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {

	@InjectMocks
	ProductController productController;

	@Mock
	ProductService productService;

	@Test
	public void productsTest() throws ProductException {
		Product product = new Product();
		product.setPrice(300D);
		product.setProductId(1);
		product.setProductName("Hoodie");
		product.setTypeId(1);

		ProductDto productDto = new ProductDto();
		productDto.setPrice(300D);
		productDto.setProductName("Hoodie");

		List<ProductDto> productList = new ArrayList<>();
		productList.add(productDto);

		Mockito.when(productService.products(Mockito.anyInt())).thenReturn(productList);
		ResponseEntity<List<ProductDto>> response= productController.products(1);
		assertNotNull(response);
	}

}
