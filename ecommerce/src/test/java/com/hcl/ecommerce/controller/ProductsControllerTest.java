package com.hcl.ecommerce.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.ecommerce.dto.ProductBuyDto;
import com.hcl.ecommerce.dto.ProductsDto;
import com.hcl.ecommerce.dto.PurchasedProductRequestDto;
import com.hcl.ecommerce.dto.PurchasedProductResponseDto;
import com.hcl.ecommerce.dto.ResponseDto;
import com.hcl.ecommerce.dto.ReviewDto;
import com.hcl.ecommerce.dto.ReviewRequestDto;
import com.hcl.ecommerce.dto.SearchResponseDto;
import com.hcl.ecommerce.exception.ProductException;
import com.hcl.ecommerce.exception.ReviewException;
import com.hcl.ecommerce.exception.SearchProductException;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.service.ProductsService;

@RunWith(MockitoJUnitRunner.class)
public class ProductsControllerTest {

	@InjectMocks
	ProductsController productsController;

	@Mock
	ProductsService productsService;

	@Test
	public void addProductTest() throws ProductException {
		ProductsDto productsDto = new ProductsDto();
		productsDto.setEmailId("haran@gmail.com");
		productsDto.setProductName("Football");
		productsDto.setQuantity(10);
		productsDto.setTypeId(3);
		productsDto.setPrice((double) 9000);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(607);

		Mockito.when(productsService.addProducts(Mockito.any())).thenReturn(responseDto);
		ResponseEntity<ResponseDto> result = productsController.addProduct(productsDto);
		assertNotNull(result);
	}
	
	/*
	 * @Test public void searchTest() throws SearchProductException { ReviewDto
	 * reviewDto = new ReviewDto(); reviewDto.setReview(1);
	 * 
	 * List<ReviewDto> reviewList = new ArrayList<>(); reviewList.add(reviewDto);
	 * 
	 * SearchResponseDto searchResponseDto = new SearchResponseDto();
	 * searchResponseDto.setPrice(600D); searchResponseDto.setProductId(1);
	 * searchResponseDto.setProductName("Pullover");
	 * searchResponseDto.setQuantity(20); searchResponseDto.setReviews(reviewList);
	 * 
	 * List<SearchResponseDto> searchList = new ArrayList<>();
	 * searchList.add(searchResponseDto);
	 * 
	 * Mockito.when(productsService.search(Mockito.anyString())).thenReturn(
	 * searchList); ResponseEntity<List<SearchResponseDto>> result =
	 * productsController.search(Mockito.anyString()); assertNotNull(result); }
	 */

	@Test
	public void reviewTest() throws ReviewException {
		ReviewRequestDto reviewRequestDto = new ReviewRequestDto();
		reviewRequestDto.setUserId(1);
		reviewRequestDto.setPurchasedProductId(1);
		reviewRequestDto.setReviews(1);
		
		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Success!!!");
		responseDto.setStatusCode(607);
		
		Mockito.when(productsService.review(Mockito.any())).thenReturn(responseDto);
		ResponseEntity<ResponseDto> result = productsController.review(reviewRequestDto);
		assertNotNull(result);
		
	}
	
	 @Test
	    public void buyProductTest() throws UserRegistrationException{
	        PurchasedProductRequestDto purchasedProductRequestDto= new PurchasedProductRequestDto();
	        purchasedProductRequestDto.setUserId(1);
	        purchasedProductRequestDto.setProductId(1);
	        purchasedProductRequestDto.setQuantity(5);
	        
	        PurchasedProductResponseDto purchasedProductResponseDto=new PurchasedProductResponseDto();
	        purchasedProductResponseDto.setMessage("Product bought successfully");
	        purchasedProductResponseDto.setStatusCode(200);
	        ProductBuyDto productDto=new ProductBuyDto();
	        productDto.setPrice(100d);
	        productDto.setProductName("Toy");
	        productDto.setTypeId(1);
	        purchasedProductResponseDto.setProducBuytDto(productDto);
	        Mockito.when(productsService.buyProduct(Mockito.any())).thenReturn(purchasedProductResponseDto);
	        ResponseEntity<PurchasedProductResponseDto> result=productsController.buyProduct(purchasedProductRequestDto);
	    
	        assertEquals(HttpStatus.OK, result.getStatusCode());
	        assertNotNull(result);
	    }

}
