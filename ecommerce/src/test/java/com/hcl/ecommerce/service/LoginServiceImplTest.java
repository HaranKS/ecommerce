package com.hcl.ecommerce.service;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.ecommerce.dto.LoginRequestDto;
import com.hcl.ecommerce.dto.LoginResponseDto;
import com.hcl.ecommerce.dto.UserDto;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.repository.UserRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LoginServiceImplTest {

	@InjectMocks
	LoginServiceImpl loginServiceImpl;

	@Mock
	UserRepository userRepository;

	LoginRequestDto loginRequestDto;
	LoginResponseDto loginResponseDto;
	User user;
	UserDto userDto;

	@Before
	public void setUp() {
		loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmailId("anu@gmail.com");
		loginRequestDto.setPassword("123");

		user = new User();
		user.setEmailId("anu@gmail.com");
		user.setName("Anuradha");
		user.setPassword("123");
		user.setTypeId(1);
		user.setUserId(1);

		userDto = new UserDto();
		userDto.setName("Anuradha");
		userDto.setTypeId(1);

		loginResponseDto = new LoginResponseDto();
		loginResponseDto.setMessage("Success");
		loginResponseDto.setStatusCode(607);
		loginResponseDto.setUserDto(userDto);

	}

	@Test
	public void loginTest() throws UserRegistrationException {
		Mockito.when(userRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.of(user));
		LoginResponseDto result = loginServiceImpl.userLogin(loginRequestDto);
		Assert.assertNotNull(result);
	}
	
	@Test(expected = UserRegistrationException.class)
	public void loginTest1() throws UserRegistrationException {
		Mockito.when(userRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.ofNullable(null));
		loginServiceImpl.userLogin(loginRequestDto);
		
	}
	
	@Test(expected = UserRegistrationException.class)
	public void loginTest2() throws UserRegistrationException {
		loginRequestDto = new LoginRequestDto();
		loginRequestDto.setEmailId("");
		loginRequestDto.setPassword("");
		Mockito.when(userRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.of(user));
		loginServiceImpl.userLogin(loginRequestDto);
		
	}

}
