package com.hcl.ecommerce.service;

import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.ecommerce.dto.ProductBuyDto;
import com.hcl.ecommerce.dto.ProductsDto;
import com.hcl.ecommerce.dto.PurchasedProductRequestDto;
import com.hcl.ecommerce.dto.PurchasedProductResponseDto;
import com.hcl.ecommerce.dto.ResponseDto;
import com.hcl.ecommerce.dto.ReviewDto;
import com.hcl.ecommerce.dto.ReviewRequestDto;
import com.hcl.ecommerce.dto.SearchResponseDto;
import com.hcl.ecommerce.entity.Product;
import com.hcl.ecommerce.entity.PurchasedProduct;
import com.hcl.ecommerce.entity.Review;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.ProductException;
import com.hcl.ecommerce.exception.ReviewException;
import com.hcl.ecommerce.exception.SearchProductException;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.repository.ProductRepository;
import com.hcl.ecommerce.repository.PurchasedProductRepository;
import com.hcl.ecommerce.repository.ReviewRepository;
import com.hcl.ecommerce.repository.UserRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductsServiceImplTest {
	@InjectMocks
	ProductsServiceImpl productsServiceImpl;
	@Mock
	ReviewRepository reviewRepository;
	@Mock
	UserRepository userRepository;
	@Mock
	PurchasedProductRepository purchasedProductRepository;
	@Mock
	ProductRepository productRepository;

	List<SearchResponseDto> searchList;
	SearchResponseDto searchResponseDto;
	List<Product> productList;
	Product product;
	List<Review> reviewList;
	Review review;
	ReviewDto reviewDto;
	List<ReviewDto> reviewDtos;
	ResponseDto responseDto;
	ReviewRequestDto reviewRequestDto;
	User user;
	ProductsDto productsDto;
	PurchasedProductRequestDto purchasedProductRequestDto;
	PurchasedProductResponseDto purchasedProductResponseDto;
	ProductBuyDto productBuyDto;
	PurchasedProduct purchasedProduct;

	@Before
	public void setUp() {
		purchasedProductRequestDto = new PurchasedProductRequestDto();
        purchasedProductRequestDto.setUserId(1);
        purchasedProductRequestDto.setProductId(1);
        purchasedProductRequestDto.setQuantity(5); 

        purchasedProductResponseDto = new PurchasedProductResponseDto();
        purchasedProductResponseDto.setMessage("Product bought successfully");
        purchasedProductResponseDto.setStatusCode(200);
        
        productBuyDto = new ProductBuyDto();
        productBuyDto.setPrice(100d);
        productBuyDto.setProductName("Toy");
        productBuyDto.setTypeId(1);
        purchasedProductResponseDto.setProducBuytDto(productBuyDto);
        
        purchasedProduct = new PurchasedProduct();
        purchasedProduct.setUserId(1);
        purchasedProduct.setProductId(1);
        purchasedProduct.setPurchasedDate(LocalDateTime.now());
        purchasedProduct.setPurchasedProductId(1);
        purchasedProduct.setQuantity(5);
		
		productsDto = new ProductsDto();
		productsDto.setEmailId("anu@gmail.com");
		productsDto.setProductName("Hoodie");
		productsDto.setQuantity(10);
		productsDto.setTypeId(1);
		productsDto.setPrice((double) 9000);

		product = new Product();
		product.setPrice(300D);
		product.setProductId(1);
		product.setProductName("Hoodie");
		product.setTypeId(1);
		product.setQuantity(5);

		productList = new ArrayList<>();
		productList.add(product);

		review = new Review();
		review.setPurchasedProductId(1);
		review.setReviews(3);
		review.setReviewDate(LocalDateTime.now());
		review.setReviewId(1);

		reviewList = new ArrayList<>();
		reviewList.add(review);

		reviewDto = new ReviewDto();
		reviewDto.setReviews(1);

		reviewDtos = new ArrayList<>();
		reviewDtos.add(reviewDto);

		searchResponseDto = new SearchResponseDto();
		searchResponseDto.setPrice(600D);
		searchResponseDto.setProductId(1);
		searchResponseDto.setProductName("Hoodie");
		searchResponseDto.setQuantity(20);
	//	searchResponseDto.setReviews(reviewDtos);

		searchList = new ArrayList<>();
		searchList.add(searchResponseDto);

		user = new User();
		user.setEmailId("anu@gmail.com");
		user.setName("Anuradha");
		user.setPassword("anu_123");
		user.setTypeId(1);
		user.setUserId(1);
		user.setRole("Admin");

		reviewRequestDto = new ReviewRequestDto();
		reviewRequestDto.setUserId(1);
		reviewRequestDto.setPurchasedProductId(1);
		reviewRequestDto.setReviews(1);

		responseDto = new ResponseDto();
		responseDto.setMessage("Success");
		responseDto.setStatusCode(607);

	}

	/*
	 * @Test public void searchTest() throws SearchProductException {
	 * Mockito.when(productRepository.findByProductName(Mockito.anyString())).
	 * thenReturn(productList);
	 * Mockito.when(reviewRepository.findByProductId(Mockito.anyInt())).thenReturn(
	 * Optional.of(reviewList)); List<SearchResponseDto> actual =
	 * productsServiceImpl.search(Mockito.anyString());
	 * Assert.assertNotNull(actual); Assert.assertEquals("Hoodie",
	 * actual.get(0).getProductName()); }
	 * 
	 * @Test(expected = SearchProductException.class) public void searchTest1()
	 * throws SearchProductException {
	 * Mockito.when(reviewRepository.findByProductId(Mockito.anyInt())).thenReturn(
	 * Optional.ofNullable(null)); productsServiceImpl.search(Mockito.anyString());
	 * }
	 */

	/*
	 * @Test public void reviewTest() throws ReviewException {
	 * Mockito.when(userRepository.findByEmailId(Mockito.anyString())).thenReturn(
	 * Optional.of(user));
	 * Mockito.when(purchasedProductRepository.findByProductId(Mockito.anyInt())).
	 * thenReturn(Optional.of(purchasedProduct));
	 * Mockito.when(reviewRepository.save(Mockito.any())).thenReturn(review);
	 * ResponseDto responseDto = productsServiceImpl.review(reviewRequestDto);
	 * Assert.assertNotNull(responseDto); }
	 */

	@Test(expected = ReviewException.class)
	public void reviewTest1() throws ReviewException {
		Mockito.when(userRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
		productsServiceImpl.review(reviewRequestDto);
	}

	/*
	 * @Test(expected = ReviewException.class) public void reviewTest2() throws
	 * ReviewException {
	 * Mockito.when(userRepository.findByEmailId(Mockito.anyString())).thenReturn(
	 * Optional.of(user));
	 * Mockito.when(purchasedProductRepository.findByProductId(Mockito.anyInt())).
	 * thenReturn(Optional.ofNullable(null));
	 * productsServiceImpl.review(reviewRequestDto); }
	 */

	@Test
	public void addProductServiceTest() throws ProductException {
		Mockito.when(userRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.of(user));
		Mockito.when(productRepository.save(Mockito.any())).thenReturn(product);
		ResponseDto result = productsServiceImpl.addProducts(productsDto);
		assertNotNull(result);
	}

	@Test(expected = ProductException.class)
	public void addProductServiceTest1() throws ProductException {
		user = new User();
		user.setUserId(1);
		user.setEmailId("lol@gmail.com");
		user.setName("Haran");
		user.setTypeId(3);
		user.setPassword("haran");
		user.setRole("Priority");
		Mockito.when(userRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.of(user));
		productsServiceImpl.addProducts(productsDto);
	}

	@Test(expected = ProductException.class)
	public void addProductServiceTest2() throws ProductException {
		user = new User();
		user.setUserId(1);
		user.setEmailId("");
		user.setName("Haran");
		user.setTypeId(3);
		user.setPassword("haran");
		user.setRole("Priority");
		Mockito.when(userRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.ofNullable(user));
		productsServiceImpl.addProducts(productsDto);

	}
	@Test
    public void buyProductTest2() throws UserRegistrationException{        
        purchasedProductRequestDto = new PurchasedProductRequestDto();
        purchasedProductRequestDto.setUserId(1);
        purchasedProductRequestDto.setProductId(1);
        purchasedProductRequestDto.setQuantity(5);
        purchasedProductResponseDto = new PurchasedProductResponseDto();
        purchasedProductResponseDto.setMessage("Product bought successfully");
        purchasedProductResponseDto.setStatusCode(200);
        productBuyDto = new ProductBuyDto();
        productBuyDto.setPrice(100d);
        productBuyDto.setProductName("Toy");
        productBuyDto.setTypeId(1);
        purchasedProductResponseDto.setProducBuytDto(productBuyDto);     
        product = new Product();
        product.setPrice(100d);
        product.setProductId(1);
        product.setProductName("Toy");
        product.setQuantity(15);
        product.setTypeId(1); 
        Mockito.when(userRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.of(user));
        Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(Optional.of(product));
        PurchasedProductResponseDto result = productsServiceImpl.buyProduct(purchasedProductRequestDto);
        assertNotNull(result);    
    }
	 @Test(expected = UserRegistrationException.class)
	    public void buyProductTest1() throws UserRegistrationException{
	        Mockito.when(userRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
	       productsServiceImpl.buyProduct(purchasedProductRequestDto);
	    }
	 @Test(expected = UserRegistrationException.class)
	    public void buyProductTests() throws UserRegistrationException{
		 Mockito.when(productRepository.findByProductId(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
	       productsServiceImpl.buyProduct(purchasedProductRequestDto);
	    }
}
