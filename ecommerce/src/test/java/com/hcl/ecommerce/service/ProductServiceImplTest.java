package com.hcl.ecommerce.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.ecommerce.dto.ProductDto;
import com.hcl.ecommerce.entity.Product;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.ProductException;
import com.hcl.ecommerce.repository.ProductRepository;
import com.hcl.ecommerce.repository.UserRepository;
@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductServiceImplTest {
	
	@InjectMocks
	ProductServiceImpl productServiceImpl;
	@Mock
	UserRepository userRepository;
	@Mock
	ProductRepository productRepository;
	
	User user;
	User user1;
	Product product;
	Product product1;
	List<Product> productList;
	ProductDto productDto;
	ProductDto productDto1;
	List<ProductDto> productDtoList;

	@Before
	public void setUp() {
		user = new User();
		user.setTypeId(1);
		user.setName("Anu");
		user.setPassword("123");
		user.setUserId(1);
		
		product = new Product();
		product.setPrice(300D);
		product.setProductId(1);
		product.setProductName("Hoodie");
		product.setTypeId(1);
				
		productList = new ArrayList<>();
		productList.add(product);
		
		productDto = new ProductDto();
		productDto.setPrice(300D);
		productDto.setProductName("Hoodie");
		
		
		productDtoList = new ArrayList<>();
		productDtoList.add(productDto);
		
	}

	@Test
	public void productsTest() throws ProductException {
		Mockito.when(userRepository.findByTypeId(Mockito.anyInt())).thenReturn(Optional.of(user));
		Mockito.when(productRepository.findAll()).thenReturn(productList);
		Mockito.when(productRepository.findByTypeId(Mockito.anyInt())).thenReturn(Optional.of(productList));
		List<ProductDto> response = productServiceImpl.products(1);
		Assert.assertNotNull(response);
		Assert.assertEquals("Hoodie", response.get(0).getProductName());
	}
	@Test(expected = ProductException.class)
	public void productsTest1() throws ProductException {
		Mockito.when(userRepository.findByTypeId(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		productServiceImpl.products(1);
	}
	@Test
	public void productsTest2() throws ProductException {
		user = new User();
		user.setTypeId(0);
		user.setName("Anuradha");
		user.setPassword("456");
		user.setUserId(2);
		product = new Product();
		product.setPrice(300D);
		product.setProductId(1);
		product.setProductName("Jacket");
		product.setTypeId(0);
		productList = new ArrayList<>();
		productList.add(product);
		productDto = new ProductDto();
		productDto.setPrice(300D);
		productDto.setProductName("Jacket");
		productDtoList = new ArrayList<>();
		productDtoList.add(productDto1);
		
		Mockito.when(userRepository.findByTypeId(Mockito.anyInt())).thenReturn(Optional.of(user));
		Mockito.when(productRepository.findAll()).thenReturn(productList);
		Mockito.when(productRepository.findByTypeId(Mockito.anyInt())).thenReturn(Optional.of(productList));
		List<ProductDto> response = productServiceImpl.products(0);
		Assert.assertNotNull(response);
	}
	@Test(expected = ProductException.class)
	public void productsTest3() throws ProductException {
		Mockito.when(productRepository.findAll()).thenReturn(null);
		productServiceImpl.products(1);
	}
	@Test(expected = ProductException.class)
	public void productsTest4() throws ProductException {
		user = new User();
		user.setTypeId(0);
		user.setName("Anuradha");
		user.setPassword("456");
		user.setUserId(2);
		product = new Product();
		product.setPrice(300D);
		product.setProductId(1);
		product.setProductName("Jacket");
		product.setTypeId(0);
		productList = new ArrayList<>();
		productList.add(product);
		productDto = new ProductDto();
		productDto.setPrice(300D);
		productDto.setProductName("Jacket");
		productDtoList = new ArrayList<>();
		productDtoList.add(productDto1);
		Mockito.when(userRepository.findByTypeId(Mockito.anyInt())).thenReturn(Optional.of(user));
		Mockito.when(productRepository.findAll()).thenReturn(productList);
		Mockito.when(productRepository.findByTypeId(Mockito.anyInt())).thenReturn(Optional.ofNullable(null));
		productServiceImpl.products(3);
	}
	

}
