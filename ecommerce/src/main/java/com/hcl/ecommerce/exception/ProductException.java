package com.hcl.ecommerce.exception;

public class ProductException extends Exception{
	private static final long serialVersionUID = 1L;

	public ProductException(String exception) {

		super(exception);
	}

}
