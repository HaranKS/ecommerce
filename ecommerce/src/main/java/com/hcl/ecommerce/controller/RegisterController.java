package com.hcl.ecommerce.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ecommerce.dto.RegisterRequestDto;
import com.hcl.ecommerce.dto.ResponseDto;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.service.RegisterService;

@RestController
@RequestMapping("/ecommerce")
@CrossOrigin(allowedHeaders = { "*", "*/" }, origins = { "*", "*/" })
public class RegisterController {
	@Autowired
	 RegisterService userService;	 

	    @PostMapping("/register")
	    public ResponseEntity<ResponseDto> register(@Valid @RequestBody RegisterRequestDto registerRequestDto) throws UserRegistrationException {
	        ResponseDto responseDto = userService.register(registerRequestDto);
	        return new ResponseEntity<>(responseDto, HttpStatus.OK);
	    }

}
