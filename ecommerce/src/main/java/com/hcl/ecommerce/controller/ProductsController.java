package com.hcl.ecommerce.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ecommerce.dto.ProductsDto;
import com.hcl.ecommerce.dto.PurchasedProductRequestDto;
import com.hcl.ecommerce.dto.PurchasedProductResponseDto;
import com.hcl.ecommerce.dto.ResponseDto;
import com.hcl.ecommerce.dto.ReviewRequestDto;
import com.hcl.ecommerce.dto.SearchResponseDto;
import com.hcl.ecommerce.exception.ProductException;
import com.hcl.ecommerce.exception.ReviewException;
import com.hcl.ecommerce.exception.SearchProductException;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.service.ProductsService;

@RestController
@RequestMapping("/products")
public class ProductsController {

	@Autowired
	ProductsService productsService;

	@GetMapping("")
	public ResponseEntity<List<SearchResponseDto>> search(@RequestParam("productName") String productName)
			throws SearchProductException {
		return new ResponseEntity<>(productsService.search(productName), HttpStatus.OK);
	}

	@PostMapping("/review")
	public ResponseEntity<ResponseDto> review(@Valid @RequestBody ReviewRequestDto reviewRequestDto) throws ReviewException {
		return new ResponseEntity<>(productsService.review(reviewRequestDto), HttpStatus.OK);
	}

	 /**
     * This method is used to Add the Products to the product list
     * *
     * @author Haran
     * @since 2020-03-26 
     * @body productDtos  -Here we are sending productDtos as request body and posting the new product
     * @return ResponseEntity Object along with status code and successfully registered
     *      
     * @throws ProductException
     */

	@PostMapping("/admin")
	public ResponseEntity<ResponseDto> addProduct(@Valid @RequestBody ProductsDto productDtos) throws ProductException {
		ResponseDto responseDto = productsService.addProducts(productDtos);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);

	}

	@PostMapping("/buy")
	public ResponseEntity<PurchasedProductResponseDto> buyProduct(@Valid
			@RequestBody PurchasedProductRequestDto purchasedProductRequestDto) throws UserRegistrationException {
		PurchasedProductResponseDto purchasedProductResponseDto = productsService
				.buyProduct(purchasedProductRequestDto);
		return new ResponseEntity<>(purchasedProductResponseDto, HttpStatus.OK);

	}

}
