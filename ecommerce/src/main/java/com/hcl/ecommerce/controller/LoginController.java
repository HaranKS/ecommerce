package com.hcl.ecommerce.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ecommerce.dto.LoginRequestDto;
import com.hcl.ecommerce.dto.LoginResponseDto;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.service.LoginService;

@RestController
@RequestMapping("/ecommerce")
@CrossOrigin(allowedHeaders = { "*", "*/" }, origins = { "*", "*/" })
public class LoginController {
	
	@Autowired
    LoginService loginService;
   
    @PostMapping("/login")
    public ResponseEntity<LoginResponseDto> userLogin(@Valid @RequestBody LoginRequestDto loginRequestDto) throws UserRegistrationException{
        LoginResponseDto loginResponseDto=loginService.userLogin(loginRequestDto);
        return new ResponseEntity<>(loginResponseDto, HttpStatus.OK);
       
    }

}
