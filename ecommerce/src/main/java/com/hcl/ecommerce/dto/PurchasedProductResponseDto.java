package com.hcl.ecommerce.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PurchasedProductResponseDto {

	private Integer statusCode;
	private String message;
	private ProductBuyDto producBuytDto;
}
