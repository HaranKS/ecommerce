package com.hcl.ecommerce.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchResponseDto {
	
	private Integer productId;
	private String productName;
	private Double price;
	private Integer quantity;
	private Double reviews;
//	List<ReviewDto> reviewsList;
	
	
}
