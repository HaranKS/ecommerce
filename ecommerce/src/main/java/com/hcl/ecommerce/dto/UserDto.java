package com.hcl.ecommerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDto {

	private String name;
	private Integer typeId;

}
