package com.hcl.ecommerce.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PurchasedProductRequestDto {

	private Integer productId;
	private Integer quantity;
	private Integer userId;

}
