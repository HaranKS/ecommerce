package com.hcl.ecommerce.dto;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewRequestDto {

	@NotNull(message = "USER ID SHOULD NOT BE NULL")
	private Integer userId;
	
	@NotNull(message = "PURCHASE PRODUCT ID SHOULD NOT BE NULL")
	private Integer purchasedProductId;
	private Integer reviews;

}
