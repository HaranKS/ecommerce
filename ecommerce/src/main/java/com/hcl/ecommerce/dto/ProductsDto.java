package com.hcl.ecommerce.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductsDto {
    
	@NotEmpty(message = "Email should not be empty")
	@Email(message = "Email should be valid")
	private String emailId;
    private String productName;
    private Double price;
    private Integer quantity;
    private Integer typeId;

 

}
