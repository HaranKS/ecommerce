package com.hcl.ecommerce.dto;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductBuyDto {
	@NotEmpty(message = "Product name should not be empty")
	private String productName;
	
	private Double price;
	private Integer typeId;
}
