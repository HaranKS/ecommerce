package com.hcl.ecommerce.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.ecommerce.constants.ApplicationConstants;
import com.hcl.ecommerce.dto.RegisterRequestDto;
import com.hcl.ecommerce.dto.ResponseDto;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.repository.UserRepository;

@Service
public class RegisterServiceImpl implements RegisterService {
	@Autowired
	UserRepository userRepository;

	@Override
	public ResponseDto register(RegisterRequestDto registerRequestDto) throws UserRegistrationException {
		User user = new User();
		ResponseDto responseDto = new ResponseDto();
		Optional<User> email = userRepository.findByEmailId(registerRequestDto.getEmailId());
		if (email.isPresent()) {
			throw new UserRegistrationException(ApplicationConstants.USER_PRESENT_ALREADY);
		}
		if (registerRequestDto.getTypeId() > 1) {
			throw new UserRegistrationException(ApplicationConstants.SELECT_PRIORITY);
		}
		BeanUtils.copyProperties(registerRequestDto, user);
		userRepository.save(user);
		responseDto.setMessage(ApplicationConstants.USER_REGISTERED);
		responseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
		return responseDto;
	}

}
