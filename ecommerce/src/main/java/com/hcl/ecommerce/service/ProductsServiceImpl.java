package com.hcl.ecommerce.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Predicate;
import com.hcl.ecommerce.constants.ApplicationConstants;
import com.hcl.ecommerce.dto.ProductBuyDto;
import com.hcl.ecommerce.dto.ProductsDto;
import com.hcl.ecommerce.dto.PurchasedProductRequestDto;
import com.hcl.ecommerce.dto.PurchasedProductResponseDto;
import com.hcl.ecommerce.dto.ResponseDto;
import com.hcl.ecommerce.dto.ReviewRequestDto;
import com.hcl.ecommerce.dto.SearchResponseDto;
import com.hcl.ecommerce.entity.Product;
import com.hcl.ecommerce.entity.PurchasedProduct;
import com.hcl.ecommerce.entity.Review;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.ProductException;
import com.hcl.ecommerce.exception.ReviewException;
import com.hcl.ecommerce.exception.SearchProductException;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.repository.ProductRepository;
import com.hcl.ecommerce.repository.PurchasedProductRepository;
import com.hcl.ecommerce.repository.ReviewRepository;
import com.hcl.ecommerce.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductsServiceImpl implements ProductsService {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	ReviewRepository reviewRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PurchasedProductRepository purchasedProductRepository;

	/**
	 * @author Anuradha
	 * 
	 *         Method is used for searching all products based on product name a
	 *         user can view all products
	 * 
	 * @param productName
	 * @return List<SearchResponseDto> that includes productName, price, productId,
	 *         quantity,and list of reviews
	 * 
	 * @throws SearchProductException
	 */
	@Override
	public List<SearchResponseDto> search(String productName) throws SearchProductException {
		List<SearchResponseDto> searchList = new ArrayList<>();
		List<Product> products = productRepository.findByProductName(productName);
		if (products.isEmpty()) {
			throw new SearchProductException(ApplicationConstants.PRODUCTS_NOT_AVAILABLE);
		}
		List<Integer> productList = products.stream().map(Product::getProductId).collect(Collectors.toList());

		log.info("productList " + productList);

		List<PurchasedProduct> purchasedProduct = purchasedProductRepository.findByProductIdIn(productList);

		Map<Integer, List<PurchasedProduct>> purchasedList = purchasedProduct.stream()
				.collect(Collectors.groupingBy(productId -> productId.getProductId()));
		log.info("purchasedList " + purchasedList);
		Map<Integer, Double> average = new HashMap<>();
		purchasedList.forEach((productId, purchasedProductList1) -> {
			List<Integer> puchasedIds = purchasedProductList1.stream().map(PurchasedProduct::getPurchasedProductId)
					.collect(Collectors.toList());
			List<Review> reviewList = reviewRepository.findByPurchasedProductIdIn(puchasedIds);
			List<Integer> ratings = reviewList.stream().map(Review::getReviews).collect(Collectors.toList());
			average.put(productId, ratings.stream().mapToInt((rating) -> rating).summaryStatistics().getAverage());
		});
		log.info("average " + average);
		products.forEach(searchProduct -> {
			SearchResponseDto searchResponseDto = new SearchResponseDto();
			searchResponseDto.setPrice(searchProduct.getPrice());
			searchResponseDto.setProductId(searchProduct.getProductId());
			searchResponseDto.setProductName(searchProduct.getProductName());
			searchResponseDto.setQuantity(searchProduct.getQuantity());
			searchResponseDto.setReviews(average.get(searchProduct.getProductId()));
			searchList.add(searchResponseDto);
		});
		return searchList;
	}

	/**
	 * @author Anuradha
	 * 
	 *         Method is used for adding reviews based on the productId n name of a
	 *         particular user
	 * 
	 * @param ReviewRequestDto that includes review, productId and reviewDate
	 * @return ResponseDto that includes custom status code and message
	 * 
	 * @throws ReviewException
	 */
	@Override
	public ResponseDto review(ReviewRequestDto reviewRequestDto) throws ReviewException {
		ResponseDto responseDto = new ResponseDto();
		Optional<User> user = userRepository.findByUserId(reviewRequestDto.getUserId());
		if (!user.isPresent()) {
			throw new ReviewException(ApplicationConstants.NOT_REGISTERED);
		}
		Optional<PurchasedProduct> purchasedProducts = purchasedProductRepository
				.findByPurchasedProductId(reviewRequestDto.getPurchasedProductId());
		if (!purchasedProducts.isPresent()) {
			throw new ReviewException(ApplicationConstants.PRODUCT_NOT_AVAILABLE_FOR_REVIEW);
		}
		if (purchasedProducts.get().getUserId() == (reviewRequestDto.getUserId())) {
			Review review = new Review();
			review.setPurchasedProductId(reviewRequestDto.getPurchasedProductId());
			review.setReviews(reviewRequestDto.getReviews());
			review.setReviewDate(LocalDateTime.now());
			reviewRepository.save(review);
			responseDto.setMessage(ApplicationConstants.REVIEW_ADDED_SUCCESSFULLY);
			responseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
		} else {
			responseDto.setMessage(ApplicationConstants.PURCHASED_PRODUCT_NOT_AVAILABLE_FOR_REVIEW);
			responseDto.setStatusCode(ApplicationConstants.UNSUCCESS_CODE);
		}

		return responseDto;
	}

	@Override
	public ResponseDto addProducts(ProductsDto productDto) throws ProductException {
		Product product = new Product();
		ResponseDto responseDto = new ResponseDto();
		Optional<User> email = userRepository.findByEmailId(productDto.getEmailId());
		if (!email.isPresent()) {
			throw new ProductException(ApplicationConstants.ADMIN_NOT_REGISTERED);
		}
		if (!email.get().getRole().equalsIgnoreCase("Admin")) {
			throw new ProductException(ApplicationConstants.USER_NOT_ADMIN);
		} else {
			BeanUtils.copyProperties(productDto, product);
			productRepository.save(product);
			responseDto.setMessage(ApplicationConstants.PRODUCT_ADDED_SUCCESSFULLY);
			responseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
		}
		return responseDto;
	}

	@Override
	public PurchasedProductResponseDto buyProduct(PurchasedProductRequestDto purchasedProductRequestDto)
			throws UserRegistrationException {
		PurchasedProduct purchasedProduct = new PurchasedProduct();
		PurchasedProductResponseDto purchasedProductResponseDto = new PurchasedProductResponseDto();
		ProductBuyDto p = new ProductBuyDto();
		Optional<User> email = userRepository.findByUserId(purchasedProductRequestDto.getUserId());
		if (!email.isPresent()) {
			throw new UserRegistrationException(ApplicationConstants.NOT_REGISTERED);
		}
		if (email.get().getRole().equalsIgnoreCase("Admin")) {
			throw new UserRegistrationException(ApplicationConstants.USER_NOT_ALLOWED_TO_BUY);
		}
		Optional<Product> product = productRepository.findByProductId(purchasedProductRequestDto.getProductId());
		if (!product.isPresent()) {
			throw new UserRegistrationException(ApplicationConstants.PRODUCTS_NOT_AVAILABLE);
		}
		if (product.get().getQuantity() < purchasedProductRequestDto.getQuantity()) {
			throw new UserRegistrationException(ApplicationConstants.QUANTITY_NOT_AVAILABLE);
		} else {
			purchasedProductResponseDto.setMessage(ApplicationConstants.PRODUCT_ADDED_SUCCESSFULLY);
			purchasedProductResponseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);

			p.setProductName(product.get().getProductName());
			p.setPrice(product.get().getPrice() * purchasedProductRequestDto.getQuantity());
			p.setTypeId(product.get().getTypeId());
			purchasedProductResponseDto.setProducBuytDto(p);
		}
		product.get().setQuantity(product.get().getQuantity() - purchasedProductRequestDto.getQuantity());
		BeanUtils.copyProperties(purchasedProductRequestDto, purchasedProduct);
		purchasedProduct.setPrice(p.getPrice());
		purchasedProduct.setPurchasedDate(LocalDateTime.now());
		purchasedProductRepository.save(purchasedProduct);
		return purchasedProductResponseDto;

	}

}
