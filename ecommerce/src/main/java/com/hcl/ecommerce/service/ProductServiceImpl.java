package com.hcl.ecommerce.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.ecommerce.constants.ApplicationConstants;
import com.hcl.ecommerce.dto.ProductDto;
import com.hcl.ecommerce.entity.Product;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.ProductException;
import com.hcl.ecommerce.repository.ProductRepository;
import com.hcl.ecommerce.repository.UserRepository;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	ProductRepository productRepository;
	@Autowired
	UserRepository userRepository;

	/**
	 * @author Anuradha
	 * 
	 *         Method is used for fetching all products based on user type
	 *         particular user can view products
	 * 
	 * @param typeId
	 * @return List<ProductDto> that includes productName and price
	 * 
	 * @throws ProductException
	 */

	@Override
	public List<ProductDto> products(Integer typeId) throws ProductException {
		List<ProductDto> productDtoList = new ArrayList<>();
		Optional<User> user = userRepository.findByTypeId(typeId);
		if (!user.isPresent()) {
			throw new ProductException(ApplicationConstants.NOT_REGISTERED);
		}
		if (user.get().getTypeId() == 1) {
			List<Product> productList = productRepository.findAll();
			productList.forEach(prod -> {
				ProductDto productDto = new ProductDto();
				productDto.setProductName(prod.getProductName());
				productDto.setPrice(prod.getPrice());
				productDto.setProductId(prod.getProductId());
				productDtoList.add(productDto);
			});
		} else {
			Optional<List<Product>> products = productRepository.findByTypeId(user.get().getTypeId());
			if (!products.isPresent()) {
				throw new ProductException(ApplicationConstants.PRODUCTS_NOT_AVAILABLE);
			}
			products.get().forEach(pro -> {
				ProductDto productDto = new ProductDto();
				productDto.setProductName(pro.getProductName());
				productDto.setPrice(pro.getPrice());
				productDto.setProductId(pro.getProductId());
				productDtoList.add(productDto);
			});
		}
		return productDtoList;
	}

}
