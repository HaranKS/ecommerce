package com.hcl.ecommerce.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.ecommerce.constants.ApplicationConstants;
import com.hcl.ecommerce.dto.LoginRequestDto;
import com.hcl.ecommerce.dto.LoginResponseDto;
import com.hcl.ecommerce.dto.UserDto;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.repository.UserRepository;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	UserRepository userRepository;

	@Override
	public LoginResponseDto userLogin(LoginRequestDto loginRequestDto) throws UserRegistrationException {
		LoginResponseDto loginResponseDto = new LoginResponseDto();
		Optional<User> user = userRepository.findByEmailIdAndPassword(loginRequestDto.getEmailId(),loginRequestDto.getPassword());
		if (!user.isPresent()) {
			throw new UserRegistrationException(ApplicationConstants.NOT_REGISTERED);
		}
		if (loginRequestDto.getEmailId().isEmpty() || loginRequestDto.getPassword().isEmpty()) {
			throw new UserRegistrationException(ApplicationConstants.INVALID_DETAILS);
		}		
		UserDto userDto = new UserDto();
		loginResponseDto.setStatusCode(ApplicationConstants.SUCCESS_CODE);
		loginResponseDto.setMessage("Logged in successfully!!..");
		userDto.setName(user.get().getName());
		userDto.setTypeId(user.get().getTypeId());
		loginResponseDto.setUserDto(userDto);

		return loginResponseDto;
	}
}
