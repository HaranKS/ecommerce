package com.hcl.ecommerce.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.ecommerce.dto.ProductDto;
import com.hcl.ecommerce.exception.ProductException;

@Service
public interface ProductService {
	
	public List<ProductDto> products(Integer typeId) throws ProductException;

}
