package com.hcl.ecommerce.service;

import org.springframework.stereotype.Service;

import com.hcl.ecommerce.dto.LoginRequestDto;
import com.hcl.ecommerce.dto.LoginResponseDto;
import com.hcl.ecommerce.exception.UserRegistrationException;

@Service
public interface LoginService {

	LoginResponseDto userLogin(LoginRequestDto loginRequestDto) throws UserRegistrationException;

}
